#!/bin/sh
tmpsvg="$(mktemp --suffix=.svg)"
base="$(basename "$1" .yml)"

mustache "$1" template.svg.mustache > "$tmpsvg" && \
	rendersvg -w 1024 -h 1024 "$tmpsvg" "${base}.png" &&
	rm "$tmpsvg"
